<?php

return [
    'adminEmail' => 'admin@example.com',
    'notice' => [
        'oncreate' => [
            'min' => '1970', // следует ввести только год
            'max' => '2017' // следует ввести только год
        ]
    ]
];

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\notice\models\NoticeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Notices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="notice-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Notice', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php if (Yii::$app->getSession()->hasFlash('succes')) : ?>
        <p class="alert alert-success">
            <?= Yii::$app->getSession()->getFlash('succes'); ?>
        </p>
    <?php endif; ?>

    <?php if (Yii::$app->getSession()->hasFlash('error_delete')) : ?>
        <p class="alert alert-danger">
            <?= Yii::$app->getSession()->getFlash('error_delete'); ?>
        </p>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
            'message:ntext',
            'oncreate:date',
                [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}'
            ],
        ],
    ]);
    ?>

    <?= \app\modules\notice\components\MontnYearWidget::widget([
        'monthYear' => $monthYear
    ]);
    ?>

</div>

<?php

namespace app\modules\notice\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\notice\models\Notice;

/**
 * NoticeSearch represents the model behind the search form about `app\modules\notice\models\Notice`.
 */
class NoticeSearch extends Notice
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['id'], 'integer'],
                [['message', 'oncreate'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $monthYear)
    {
        $query = Notice::find();

        if ($monthYear) {
            $query->where(['DATE_FORMAT(`oncreate`, \'%m%Y\')' => $monthYear]);
        }

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['oncreate' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'oncreate' => $this->oncreate,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }

}

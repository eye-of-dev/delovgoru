<?php

namespace app\modules\notice\models;

use Yii;

/**
 * This is the model class for table "notice".
 *
 * @property integer $id
 * @property string $message
 * @property string $oncreate
 */
class Notice extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
                [['message'], 'required'],
                [['message'], 'string'],
                [['oncreate'], 'default', 'value' => function ($model) {
                    if (
                            isset(Yii::$app->params['notice']['oncreate']['min']) &&
                            !empty(Yii::$app->params['notice']['oncreate']['min']) &&
                            isset(Yii::$app->params['notice']['oncreate']['max']) &&
                            !empty(Yii::$app->params['notice']['oncreate']['max'])
                    ) {
                        /**
                         * TODO: вывести в отдельный helper
                         */
                        $datestart = strtotime(Yii::$app->params['notice']['oncreate']['min'] . '-01-01');
                        $dateend = strtotime(Yii::$app->params['notice']['oncreate']['max'] . '-12-31');
                        $daystep = 86400;
                        $datebetween = abs(($dateend - $datestart) / $daystep);
                        $randomday = rand(0, $datebetween);

                        return date('Y-m-d', $datestart + ($randomday * $daystep));
                    } else {
                        return date('Y-m-d');
                    }
                }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'message' => 'Сообщение',
            'oncreate' => 'Дата создания',
        ];
    }

    /**
     * Получаем уникальную пару месяц-год
     * @return object
     */
    public static function getMontnYear()
    {
        return self::find()->groupBy(['DATE_FORMAT(`oncreate`, \'%m-%Y\')'])->orderBy(['oncreate' => SORT_ASC])->all();
    }

}

<?php

namespace app\modules\notice\components;

use app\modules\notice\models\Notice;
use yii\base\Widget;

class MontnYearWidget extends Widget
{

    public $monthYear = null;

    public function run()
    {
        $results = Notice::getMontnYear();

        return $this->render('montnYearWidget', ['results' => $results, 'monthYear' => $this->monthYear]);
    }

}

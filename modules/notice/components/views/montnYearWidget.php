<?php

    use Yii;
    use yii\helpers\Url;
?>
<ul class="pagination">
<?php foreach ($results as $result) : ?>
    <li class="<?= (date('mY', strtotime($result->oncreate)) == $monthYear) ? 'active' : ''; ?>">
        <a href="<?= Url::to(['/notice/default/index', 'monthYear' => date('mY', strtotime($result->oncreate))]); ?>">
            [<?= Yii::$app->formatter->asDate($result->oncreate, 'LLL yyyy'); ?>]
        </a>
    </li>
<?php endforeach; ?>
</ul>

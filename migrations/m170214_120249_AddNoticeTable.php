<?php

use yii\db\Migration;

class m170214_120249_AddNoticeTable extends Migration
{

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        //Notice table
        $this->createTable('notice', [
            'id' => 'pk',
            'message' => $this->text(),
            'oncreate' => $this->timestamp()->defaultValue('0000-00-00 00:00:00')
                ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('notice');
    }

    /*
      // Use safeUp/safeDown to run migration code within a transaction
      public function safeUp()
      {
      }

      public function safeDown()
      {
      }
     */
}
